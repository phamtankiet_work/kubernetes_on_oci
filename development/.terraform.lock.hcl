# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.4.0"
  hashes = [
    "h1:R97FTYETo88sT2VHfMgkPU3lzCsZLunPftjSI5vfKe8=",
    "zh:53604cd29cb92538668fe09565c739358dc53ca56f9f11312b9d7de81e48fab9",
    "zh:66a46e9c508716a1c98efbf793092f03d50049fa4a83cd6b2251e9a06aca2acf",
    "zh:70a6f6a852dd83768d0778ce9817d81d4b3f073fab8fa570bff92dcb0824f732",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:82a803f2f484c8b766e2e9c32343e9c89b91997b9f8d2697f9f3837f62926b35",
    "zh:9708a4e40d6cc4b8afd1352e5186e6e1502f6ae599867c120967aebe9d90ed04",
    "zh:973f65ce0d67c585f4ec250c1e634c9b22d9c4288b484ee2a871d7fa1e317406",
    "zh:c8fa0f98f9316e4cfef082aa9b785ba16e36ff754d6aba8b456dab9500e671c6",
    "zh:cfa5342a5f5188b20db246c73ac823918c189468e1382cb3c48a9c0c08fc5bf7",
    "zh:e0e2b477c7e899c63b06b38cd8684a893d834d6d0b5e9b033cedc06dd7ffe9e2",
    "zh:f62d7d05ea1ee566f732505200ab38d94315a4add27947a60afa29860822d3fc",
    "zh:fa7ce69dde358e172bd719014ad637634bbdabc49363104f4fca759b4b73f2ce",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version     = "5.1.0"
  constraints = ">= 4.118.0"
  hashes = [
    "h1:2ubvPxkChmLqtz4MIuWejvHFfdKtn9c8HcbCOw9FmeQ=",
    "zh:02221e2ec03f7e65d66b2bfff9a2e40266faf46729662545786a13da663f77d1",
    "zh:0f38f49bacc9acfa5407b03515560acf29082057c68b02b2f346f0afd06fcb72",
    "zh:30b0535f8efbe992a59d01e6c2c9abda78d390c70d089e7d825771c865e5ccdf",
    "zh:44bc8cd692bd9d44380c05971654082edc76a56eee9b4a03d0fb88df1fd43298",
    "zh:458784e4f9c0ae8487f0c3f85529213913b9b988c42d02f4b8a68c2cb3f3763b",
    "zh:4ca74affb3d09a5b7b849859c9649b5ef3c6e2211f11dc228842cf7b2a5d9751",
    "zh:58858d7636eea7ab22832995e9e9d04ef44fad9b4b850dc96aaef8ca98299473",
    "zh:7ce0fcba8ca37d4d8ef8a4fbf364dae761ce29073da5e2e80ce4aec0684ad51a",
    "zh:8341aed06ffa2d96d89ceaa199584ea6f8d4503aab4b9968ab092a365f79603c",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:aa4f9b6870c0084efefd04dcff1d6d414687b46acc12fa874ab1c454c5050715",
    "zh:aacad63078ceee12d2dcf6a79f408294fe52369d4dc023413c1866055408507c",
    "zh:d18f28f63b67ab96d77c6af237cd1dda8b392c2ef81b28da470f25454fe3853a",
    "zh:f1cb979e8a46059422e7d3ae4beb7b5076a2bcbbdd728b9006d4765e62cbaeec",
    "zh:f9918cd8d299dc5bf1258c1efbf09c8002718508e47157904120862df2701f44",
  ]
}
