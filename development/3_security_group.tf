# ICMP ("1"), TCP ("6"), UDP ("17"), and ICMPv6 ("58")
resource "oci_core_security_list" "k8s_network_security_list" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id
  display_name   = "k8s_network_security_list"

  ingress_security_rules {
    protocol    = "6"
    source      = "0.0.0.0/0"
    description = "Allow all inbound traffic from to kube-apiserver"
    tcp_options {
      min = 6443
      max = 6443
    }
  }

}

resource "oci_core_security_list" "inter_subnet_security_list" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id
  display_name   = "k8s_security_list"

  ingress_security_rules {
    protocol    = "all"
    source      = var.k8s_subnet_ipv4_cidr
    description = "Allow all inbound traffic from subnet"
  }
}

resource "oci_core_security_list" "outbound_security_list" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id
  display_name   = "outbound_security_list"

  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "all"
    description = "Allow all outbound traffic"
  }
}

resource "oci_core_security_list" "http_https_security_list" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id
  display_name   = "http_https_security_list"

  ingress_security_rules {
    protocol    = "6"
    source      = "0.0.0.0/0"
    description = "Allow all tcp incoming traffic on port 80"
    tcp_options {
      min = 80
      max = 80
    }
  }

  ingress_security_rules {
    protocol    = "6"
    source      = "0.0.0.0/0"
    description = "Allow all tcp ncoming traffic on port 443"
    tcp_options {
      min = 443
      max = 443
    }
  }
}

resource "oci_core_security_list" "ssh_security_list" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id
  display_name   = "ssh_security_list"

  ingress_security_rules {
    protocol    = "6"
    source      = "0.0.0.0/0"
    description = "Allow all tcp incoming traffic on port 22"
    tcp_options {
      min = 22
      max = 22
    }
  }
}


