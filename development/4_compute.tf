data "oci_core_images" "arm_images" {

  compartment_id           = var.compartment_id
  operating_system         = "Canonical Ubuntu"
  operating_system_version = "22.04"
  shape                    = "VM.Standard.A1.Flex"
}

data "oci_identity_availability_domains" "k8s_avalability_domains" {
  compartment_id = var.compartment_id
}

resource "oci_core_instance" "k8s_control_plane" {
  availability_domain = data.oci_identity_availability_domains.k8s_avalability_domains.availability_domains[var.availability_domain].name
  compartment_id      = var.compartment_id
  shape               = "VM.Standard.A1.Flex"
  display_name        = "k8s_control_plane"

  shape_config {
    memory_in_gbs = 12
    ocpus         = 2
  }

  metadata = {
    ssh_authorized_keys = file(var.public_ssh_key_path)
    user_data           = filebase64("${path.module}/scripts/init.sh")
  }

  create_vnic_details {
    assign_public_ip          = true
    subnet_id                 = oci_core_subnet.k8s_subnet.id
    assign_private_dns_record = true
    # hostname_label            = "k8s_control_manager"
  }

  source_details {
    #Required
    source_id               = data.oci_core_images.arm_images.images[0].id
    source_type             = "image"
    boot_volume_size_in_gbs = var.ampere_boot_volume_size
  }
}

resource "oci_core_instance" "k8s_worker" {

  availability_domain = data.oci_identity_availability_domains.k8s_avalability_domains.availability_domains[var.availability_domain].name
  compartment_id      = var.compartment_id
  shape               = "VM.Standard.A1.Flex"
  display_name        = "k8s_worker"

  shape_config {
    memory_in_gbs = 12
    ocpus         = 2
  }

  metadata = {
    ssh_authorized_keys = file(var.public_ssh_key_path)
    user_data           = filebase64("${path.module}/scripts/init.sh")
  }

  create_vnic_details {
    assign_public_ip          = true
    subnet_id                 = oci_core_subnet.k8s_subnet.id
    assign_private_dns_record = true
    # hostname_label            = "k8s_worker"
  }

  source_details {
    #Required
    source_id               = data.oci_core_images.arm_images.images[0].id
    source_type             = "image"
    boot_volume_size_in_gbs = var.ampere_boot_volume_size
  }
}

resource "local_file" "ansible_inventory" {
  filename = "${path.module}/ansible/inventory.ini"

  content = join("\n", [
    "[control_plane]",
    "${oci_core_instance.k8s_control_plane.public_ip} ansible_user=${var.nodes_user} ansible_ssh_private_key_file=${var.private_ssh_key_path}",
    "[worker]",
    "${oci_core_instance.k8s_worker.public_ip} ansible_user=${var.nodes_user} ansible_ssh_private_key_file=${var.private_ssh_key_path}",
  ])
}
