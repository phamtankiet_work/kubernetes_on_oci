variable "tenancy_ocid" {
  description = "The OCID of the tenancy to create the instance in."
}

variable "user_ocid" {
  description = "The OCID of the user to authenticate with."
}

variable "fingerprint" {
  description = "The fingerprint of the API signing key."
}

variable "private_key_path" {
  description = "The local path to the private API signing key."
}

variable "region" {
  description = "The region to create the instance in."
}

variable "compartment_id" {
  description = "The OCID of the compartment to create the instance in."
}

variable "k8s_vcn_ipv4_cidr" {
  default     = "10.0.0.0/16"
  description = "IPv4 CIDR"
}

variable "k8s_subnet_ipv4_cidr" {
  default     = "10.0.1.0/24"
  description = "IPv4 CIDR for"
}

variable "public_ssh_key_path" {
  description = "The local path to the ssh public key for ssh"
}

variable "private_ssh_key_path" {
  description = "The local path to the ssh private key for ssh"
}
variable "ampere_boot_volume_size" {
  description = "Size of the boot volume in GBs"
  type        = number
}

variable "availability_domain" {
  description = "Availability Domain to launch the compute instances in. Mostly 0, 1 or 2"
  type        = number
  default     = 0
}

variable "nodes_user" {
  description = "SSH User"
}

# variable "k8s_manager_count" {
#   description = "Number of managers"
#   default     = 1
# }

# variable "k8s_worker_count" {
#   description = "Number of workers"
#   default     = 1
# }

variable "additional_tags" {
  default     = { "terraform" = "true", "terraform_id" = "k8s_1" }
  description = "Additional resource tags"
  type        = map(string)
}
