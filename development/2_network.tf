resource "oci_core_vcn" "k8s_vcn" {
  compartment_id = var.compartment_id

  cidr_block   = var.k8s_vcn_ipv4_cidr
  display_name = "k8s_vcn"

}

resource "oci_core_internet_gateway" "k8s_internet_gateway" {
  display_name   = "k8s_internet_gateway"
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id

}

resource "oci_core_route_table" "k8s_route_table" {
  display_name   = "k8s_route_table"
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id

  route_rules {
    destination       = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.k8s_internet_gateway.id
  }
}

resource "oci_core_subnet" "k8s_subnet" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.k8s_vcn.id
  cidr_block     = var.k8s_subnet_ipv4_cidr

  display_name = "k8s_subnet"

  security_list_ids = [
    oci_core_security_list.k8s_network_security_list.id,
    oci_core_security_list.inter_subnet_security_list.id,
    oci_core_security_list.outbound_security_list.id,
    oci_core_security_list.http_https_security_list.id,
    oci_core_security_list.ssh_security_list.id
  ]
}

resource "oci_core_route_table_attachment" "k8s_route_table_k8s_subnet_association" {
  route_table_id = oci_core_route_table.k8s_route_table.id
  subnet_id      = oci_core_subnet.k8s_subnet.id
}

