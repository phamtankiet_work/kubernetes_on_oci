k8s_vcn_ipv4_cidr    = "10.1.0.0/16"
k8s_subnet_ipv4_cidr = "10.1.1.0/24"

# replace with relative path of ssh key pair
public_ssh_key_path  = ".secret.ssh.key.pub"
private_ssh_key_path = ".secret.ssh.key"

# If you want to use a different AZ to launch instances in you can specify it here. The AZs are automatically sourced into a data resource. All you need to do in input the index. You want the value to be 0, 1 or 2. 
availability_domain = "0"

# Specify the disk size in GBs of the nodes in your cluster. Keep them <100 to stay within the Free Tier
ampere_boot_volume_size = "50"

nodes_user = "ubuntu"

# swarm_worker_count = 1

# swarm_manager_count = 1

# swarm_manager_image_id = "	ocid1.image.oc1.ap-singapore-1.aaaaaaaalqswflxlwzjnbqmz2bn3dsrsq37leqady6nu5n67zuwtgopcrnia"
